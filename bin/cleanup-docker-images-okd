#!/bin/bash


# Example usage:
# oc login https://manage.cloud.duke.edu --token=<token>
# oc project <project>
# cleanup-docker-images-okd locksmith -f pipeline- --keep-current -k 15

set -e

KEEP_NUMBER=5
keep_current=false

no_matches() {
  echo "No matching images found"
  exit 0
}

usage() {
  echo "Usage: $0 <image> [--keep-current] [--keep-tag <tag>,..] [--keep <number>] [--filter <partial string>]"
  echo
  echo "  --keep-current     - Keep any tags used by current pods"
  echo "  --keep-tag <tag>   - Do not delete the specified tag."
  echo "                       May be specified multipled times."
  echo "  --keep <number>    - Number of tags to keep in addition to --keep-tag tags."
  echo "                       (default: ${KEEP_NUMBER})"
  echo "  --filter <partial str> - Only consider tags that match the partial string"
  echo
  echo "This will remove the oldest tags available for a given docker image."
}

keepTags=()

OPTS=$(getopt -l keep-current,keep-tag:,keep:,filter: -n ${0} -o k:f: --  "$@") || exit 1
eval set -- "$OPTS"
while true; do
  case "${1}" in
    --keep-current) keep_current=true ;;
    --keep-tag)     keepTags+=(${2}) ; shift ;;
    --keep|-k)      KEEP_NUMBER=${2} ; shift ;;
    --filter|-f)    FILTER=${2}      ; shift ;;
    --)             shift; break;;
    *)              echo "Unknown argument: ${1}"; exit 1;;
  esac
  shift
done

if [ $# -ne 1 ]; then
  usage
  exit 1
fi

IMAGE=$1

echo "Cleaning up old tags for ${IMAGE}"

TAGS=$(kubectl get imagestreamtag -o custom-columns=TIME:.metadata.creationTimestamp,NAME:.metadata.name|tail -n +2|sort -n|awk '{print $2}')
TAGS=$(echo "${TAGS}" | grep ^${IMAGE}: || true)

if [ -z "${TAGS}" ]; then
  no_matches
fi

echo "Considering $(echo "${TAGS}" | wc -l) tags for ${IMAGE}"
for tag in ${keepTags[@]}; do
  echo "Removing '${tag}' from tags to consider"
  TAGS=$(echo "${TAGS}" | grep -vx ${IMAGE}:${tag})
done

if [ -n ${FILTER+x} ]; then
  echo "Filtering to tags matching ${FILTER}"
  TAGS=$(echo "${TAGS}" | grep ^${IMAGE}:${FILTER} || true)
fi

if [ -z "${TAGS}" ]; then
  no_matches
fi

if [ "$keep_current" = true ]; then
  echo "Looking for tags from pods to keep"
  registry_base=$(kubectl get imagestream ${IMAGE} -o jsonpath="{.status.dockerImageRepository}")
  images_in_use=$(kubectl get pod -o jsonpath="{.items[*].spec.containers[*].image}" | tr -s '[[:space:]]' '\n'  | sort -u || true)
  images_in_use=$(echo "${images_in_use}" | grep ^${registry_base}: || true)
  tags_in_use=$(echo "${images_in_use}" | sed s,^${registry_base}:,,)
  for tag in ${tags_in_use[@]}; do
    echo "Removing '${tag}' from tags to consider (it is currently in use)"
    TAGS=$(echo "${TAGS}" | grep -vx ${IMAGE}:${tag}  || true)
  done

  if [ -z "${TAGS}" ]; then
    no_matches
  fi
fi

echo "Removing most recent ${KEEP_NUMBER} tags from list"
TAGS=$(echo "${TAGS}" | head -n -${KEEP_NUMBER})

echo
for tag in ${TAGS}; do
  echo "Removing tag ${tag}"
  kubectl delete imagestreamtag ${tag}
done
