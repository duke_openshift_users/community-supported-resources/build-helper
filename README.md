*Note: To use this build helper, most people simply need to add the following to their `.gitlab-ci.yml` file*

```yaml
image:
  name: gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/build-helper:alpine
```

After adding this, you'll have `git`, `helm`, `kubectl`, `vault`, `crane`, and other tools.
See [Dockerfile](./Dockerfile) for specifics on what is installed.

## Alpine Version

A version of this is image is available based on alpine linux.  The goal of the alpine
version is to shrink the image size in order to give a slight boost to the setup time
of your gitlab-ci jobs.

To use the alpine version, add the following to your .gitlab-ci.yml:

```yaml
image:
  name: gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/build-helper:alpine
```

#### Docker registry cleanup

The alpine image also includes the [cleanup-docker-image](cleanup-docker-images.md) scripts

#### oc wrapper
Due to the size of the `oc` binary, it has been dropped in favor of a wrapper script around `kubectl`.

The `oc` wrapper supports the following calls:
```bash
oc login ${K8S_URL} --token=<token>
```
```bash
oc project ${NAMESPACE_NAME}
```

All other calls to `oc` will be passed onto `kubectl` with the same arguments


### Alpine Versions

There are multiple alpine tags to choose from.  The following chart gives an overview
of the versions of software installed on them.   If you use the `:alpine` tag, you'll get
latest alpine build.

| | :alpine3.12 | :alpine3.16 |
| --- | --- | --- |
| docker | 19.03.9 | 20.10.17 |
| helm | 3.4.2 | 3.9.0 |
| kubectl | 1.53.3 | 1.24.2 |
| crane | 0.3.0 | 0.10.0 |

## Ubuntu Version

This is the original version of this image.  It contains a base ubuntu images as well
as extra tools like `git`, `helm`, `oc`, `kubectl`, and `vault`.

To use this version, add the following to your .gitlab-ci.yml:

```yaml
image:
  name: gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/build-helper:ubuntu22
```
