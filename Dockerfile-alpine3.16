FROM alpine:3.16

# ensure there is always a usable ${HOME}
RUN mkdir -p /home/user && chmod 777 /home/user
ENV HOME=/home/user


RUN apk add --no-cache curl skopeo git bash openssh-client unzip jq binutils vault \
                       gettext tzdata

ENV CURL_CMD="curl -f -L -y 1 --retry 5"

# Install docker CLI (the apk dependencies aren't worth the apk install since we want a
# small image)
ENV DOCKER_VERSION=20.10.17
RUN ${CURL_CMD} -o /tmp/docker.tgz https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz && \
    tar -xvzf /tmp/docker.tgz -C /usr/local/bin --strip=1 docker/docker && \
    strip /usr/local/bin/docker && \
    rm -f /tmp/docker.tgz

ENV HELM_ARCH=linux-amd64

# Install helm3
ENV HELM3_VERSION=3.9.0
RUN ${CURL_CMD} -o /tmp/helm.tgz https://get.helm.sh/helm-v${HELM3_VERSION}-${HELM_ARCH}.tar.gz && \
    tar xvzf /tmp/helm.tgz -C /usr/local/bin --strip=1 ${HELM_ARCH}/helm && \
    ln -s /usr/local/bin/helm /usr/local/bin/helm3 && \
    rm -f /tmp/helm.tgz

# Install kubectl
ENV KUBECTL_VERSION=1.24.2
RUN ${CURL_CMD} -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod 755 /usr/local/bin/kubectl

# Install crane
ENV CRANE_VERSION=0.10.0
RUN ${CURL_CMD} -o /tmp/crane.tgz https://github.com/google/go-containerregistry/releases/download/v${CRANE_VERSION}/go-containerregistry_Linux_x86_64.tar.gz && \
    tar -xvzf /tmp/crane.tgz -C /usr/local/bin crane && \
    strip /usr/local/bin/crane && \
    rm -f /tmp/crane.tgz

ADD bin/oc-wrapper /usr/local/bin/oc
ADD bin/cleanup-docker-images /usr/local/bin/cleanup-docker-images
ADD bin/cleanup-docker-images-okd /usr/local/bin/cleanup-docker-images-okd
