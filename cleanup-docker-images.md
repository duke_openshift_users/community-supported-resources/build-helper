# Cleaning up images in docker regitries

This image includes two scripts to help you cleanup docker images in a registry.
These are useful to run after a new deploy or CI build.


## cleanup-docker-images

This script uses `crane` to inspect the available images and prune them.

You must use `crane auth login` to login to the registry before calling this script.

Example usage:
```bash
# login to registry
crane auth login registry.example.com -u <username> -p <password>
# Cleanup old tags for registry.example.com/myimage
# Only cleanup tags that include 'ci-job'
# Make sure the tag 'ci-job-42' will not be removed
# Make sure the tag 'ci-job-25' will not be removed
# Keep the 10 most recent tags (in addition to ci-job-42 and ci-job-25)
cleanup-docker-images registry.example.com/myimage --filter ci-job- \
  --keep-tag ci-job-42 --keep-tag ci-job-25 --keep 10
```


## cleanup-docker-images-okd

OKD/OpenShift does not allow users to delete registry images using the registry API.

This script uses `oc` to prune `imagestreamtag` objects which will prune the registry
images.  Before calling this script, you must call `oc login` and `oc project`.

Example usage:
```bash
# login to OKD
oc login https://manage.cloud.duke.edu --token=<token>
# Go to the right project/namespace
oc project <myapp>
# Cleanup old tags for myimage (aka registry.cloud.duke.edu/myapp/myimage)
# Only cleanup tags that include 'ci-job'
# Make sure the tag 'ci-job-42' will not be removed
# Make sure the tag 'ci-job-25' will not be removed
# Keep the 10 most recent tags (in addition to ci-job-42 and ci-job-25)
cleanup-docker-images-okd myimage --filter ci-job- --keep-tag ci-job-42 \
  --keep-tag ci-job-25 --keep 10
```
